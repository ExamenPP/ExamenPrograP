﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocket : MonoBehaviour {

	// Use this for initialization
	void Start () {

		List<Items> items = new List<Items> ();

		items.Add (new Items ("Helmet", 1));
		items.Add (new Items ("Armor", 2));
		items.Add (new Items ("Potion", 3));

		items.Sort ();

		foreach (Items _item in items) {
			print (_item.name + " " + _item.stock);
		}	

		items.Clear ();
	
	}
}
