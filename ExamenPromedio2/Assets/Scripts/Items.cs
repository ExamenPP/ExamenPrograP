﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Items : MonoBehaviour, IComparable<Items> {

	public string name;
	public int stock;

	public Items(string name, int stock)
	{
		string _name = name;
		int _stock = stock;
	}

	public int CompareTo(Items other)
	{
		if (other == null){

			return 1;
		}

		return stock - other.stock;
	}
}
